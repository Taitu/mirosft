var total = 600;

var subtotalLogin = 0;
var subtotalSocial = 0;
var subtotalCommon = 0;
var subtotalDevice = 0;
var subtotalEcommerce = 0;

var rSubtotalLogin = 0;
var rSubtotalSocial = 0;
var rSubtotalCommon = 0;
var rSubtotalDevice = 0;
var rSubtotalEcommerce = 0;
var rTotal = 0;

var platformCount = 1;
var factorCount = 1;

$(document).ready(function() {
    $('.tab-list__item a').on('click', function(e) {
        e.preventDefault();
        $('.tab-list__item.active').removeClass('active');
        $(this).closest('.tab-list__item').addClass('active');

        var target = $(this).data('target');
        $('.tab.active').removeClass('active');
        $(target).addClass('active');

        $('video').each(function() {
            $(this).get(0).pause();
        });
    });

    $('.features__item-not[data-factor="platform"]').on('click', function() {
        $(this).toggleClass('clicked');
        platformCount = $('.features__item-not.clicked[data-factor="platform"]').length;
        if ($(this).hasClass('visible')) {
            total -= $(this).data('price');
            if (platformCount > 1) {
                recalc(platformCount, false);
            }
            if (total < 600) {
                total = 600;
            }
        }
        else {
            if (platformCount == 1) {
                total = 600;
            }
            else {
                total += $(this).data('price');
                if (platformCount > 1) {
                    recalc(platformCount, true);
                }
            }
        }

        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);
    });

    $('.features__item-not[data-factor="factor"]').on('click', function() {
        $(this).toggleClass('clicked');
        factorCount = $('.features__item-not.clicked[data-factor="factor"]').length;
        if ($(this).hasClass('visible')) {
            if (factorCount > 1) {
                recalcFactor(factorCount, false);
            }
        }
        else {
            if (factorCount > 1) {
                recalcFactor(factorCount, true);
            }
        }

    });

    $('.features__item').not('.features__item-not').on('click', function(e) {
        $(this).toggleClass('clicked');
        e.preventDefault();
        e.stopPropagation();


        if ($(this).hasClass('visible')) {
            total -= (subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce);
            if ($(this).data('parent') == 'login') {
                subtotalLogin -= $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'social') {
                subtotalSocial -= $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'common') {
                subtotalCommon -= $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'device') {
                subtotalDevice -= $(this).data('price') * platformCount;

            }
            if ($(this).data('parent') == 'ecommerce') {
                subtotalEcommerce -= $(this).data('price') * platformCount;
            }

        }
        else {
            if ($(this).data('parent') == 'login') {
                subtotalLogin += $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'social') {
                subtotalSocial += $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'common') {
                subtotalCommon += $(this).data('price') * platformCount;
            }
            if ($(this).data('parent') == 'device') {
                subtotalDevice += $(this).data('price') * platformCount;

            }
            if ($(this).data('parent') == 'ecommerce') {
                subtotalEcommerce += $(this).data('price') * platformCount;
            }

            total += subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce;
        }

        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);

    });

    $('.features__item').on('click', function() {
        $(this).toggleClass('visible');
    });

    $('.enquire-price__title').on('click', function() {
        $(this).toggleClass('collapse');
        $('.detailed-pricing').toggleClass('collapse');
    });

    $('.nav__main .nav__link').on('click', function(e) {
        e.preventDefault();
        if ($(this).data('target')) {
            $('html, body').animate({
                scrollTop: $($(this).data('target')).offset().top
            }, 1000);
        }
    });

    $('.nav__enquire .nav__link').on('click', function(e) {
        e.preventDefault();
        if ($(this).data('target')) {
            $('html, body').animate({
                scrollTop: $($(this).data('target')).offset().top - 100
            }, 1000);
        }
    });

    $('.enquire-price__buy').on('click', function(e) {
        e.preventDefault();
        $('.modal').show();
        $('body').addClass('no-overflow').append('<div class="modal-backdrop in"/>');
    });

    $('.modal').on('click', '.close', function(e) {
        e.preventDefault();
        $('.modal').hide();
        $('body').removeClass('no-overflow');
        $('.modal-backdrop').remove();
    });
});

function showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total) {
    $('.detailed-pricing__value-sum.login').text(subtotalLogin);
    $('.detailed-pricing__value-sum.social').text(subtotalSocial);
    $('.detailed-pricing__value-sum.common').text(subtotalCommon);
    $('.detailed-pricing__value-sum.device').text(subtotalDevice);
    $('.detailed-pricing__value-sum.ecommerce').text(subtotalEcommerce);
    $('.enquire-price__value').text(total);
}

function recalc(platformCount, action) {

    if (action) {
        $('.enquire-block__login').find('.features__item.visible').each(function() {
            rSubtotalLogin += $(this).data('price') * platformCount;
            subtotalLogin = rSubtotalLogin;
        });
        $('.enquire-block__social').find('.features__item.visible').each(function() {
            rSubtotalSocial += $(this).data('price') * platformCount;
            subtotalSocial = rSubtotalSocial;
        });
        $('.enquire-block__common').find('.features__item.visible').each(function() {
            rSubtotalCommon += $(this).data('price') * platformCount;
            subtotalCommon = rSubtotalCommon;
        });
        $('.enquire-block__device').find('.features__item.visible').each(function() {
            rSubtotalDevice += $(this).data('price') * platformCount;
            subtotalDevice = rSubtotalDevice;
        });
        $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
            rSubtotalEcommerce += $(this).data('price') * platformCount;
            subtotalEcommerce = rSubtotalEcommerce;
        });
        total += subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce;
        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);
    }
    else if (!action) {
        $('.enquire-block__login').find('.features__item.visible').each(function() {
            rSubtotalLogin -= $(this).data('price') * platformCount;
            subtotalLogin = rSubtotalLogin;
        });
        $('.enquire-block__social').find('.features__item.visible').each(function() {
            rSubtotalSocial -= $(this).data('price') * platformCount;
            subtotalSocial = rSubtotalSocial;
        });
        $('.enquire-block__common').find('.features__item.visible').each(function() {
            rSubtotalCommon -= $(this).data('price') * platformCount;
            subtotalCommon = rSubtotalCommon;
        });
        $('.enquire-block__device').find('.features__item.visible').each(function() {
            rSubtotalDevice -= $(this).data('price') * platformCount;
            subtotalDevice = rSubtotalDevice;
        });
        $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
            rSubtotalEcommerce -= $(this).data('price') * platformCount;
            subtotalEcommerce = rSubtotalEcommerce;
        });

        total -= (subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce);
        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);
    }
}

function recalcFactor(factorCount, action) {
    if (action) {
        if (factorCount == 2) {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin += $(this).data('price') * 1.5;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial += $(this).data('price') * 1.5;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon += $(this).data('price') * 1.5;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice += $(this).data('price') * 1.5;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce += $(this).data('price') * 1.5;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }
        else if(factorCount == 3) {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin += $(this).data('price') * 2;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial += $(this).data('price') * 2;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon += $(this).data('price') * 2;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice += $(this).data('price') * 2;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce += $(this).data('price') * 2;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }
        else {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin += $(this).data('price') * factorCount;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial += $(this).data('price') * factorCount;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon += $(this).data('price') * factorCount;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice += $(this).data('price') * factorCount;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce += $(this).data('price') * factorCount;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }

        total += subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce;
        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);
    }
    else if (!action) {
        if (factorCount == 2) {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin -= $(this).data('price') * 1.5;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial -= $(this).data('price') * 1.5;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon -= $(this).data('price') * 1.5;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice -= $(this).data('price') * 1.5;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce -= $(this).data('price') * 1.5;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }
        else if(factorCount == 3) {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin -= $(this).data('price') * 2;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial -= $(this).data('price') * 2;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon -= $(this).data('price') * 2;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice -= $(this).data('price') * 2;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce -= $(this).data('price') * 2;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }
        else {
            $('.enquire-block__login').find('.features__item.visible').each(function() {
                rSubtotalLogin -= $(this).data('price') * factorCount;
                subtotalLogin = rSubtotalLogin;
            });
            $('.enquire-block__social').find('.features__item.visible').each(function() {
                rSubtotalSocial -= $(this).data('price') * factorCount;
                subtotalSocial = rSubtotalSocial;
            });
            $('.enquire-block__common').find('.features__item.visible').each(function() {
                rSubtotalCommon -= $(this).data('price') * factorCount;
                subtotalCommon = rSubtotalCommon;
            });
            $('.enquire-block__device').find('.features__item.visible').each(function() {
                rSubtotalDevice -= $(this).data('price') * factorCount;
                subtotalDevice = rSubtotalDevice;
            });
            $('.enquire-block__ecommerce').find('.features__item.visible').each(function() {
                rSubtotalEcommerce -= $(this).data('price') * factorCount;
                subtotalEcommerce = rSubtotalEcommerce;
            });
        }

        total -= (subtotalLogin + subtotalSocial + subtotalCommon + subtotalDevice + subtotalEcommerce);
        showTotal(subtotalLogin, subtotalSocial, subtotalCommon, subtotalDevice, subtotalEcommerce, total);
    }
}